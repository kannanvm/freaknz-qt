/*
Freaknz-qt: A QT application to convert Malayalam ASCII to Unicode format.
Copyright (C) Kannan V M 2019

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "asciitounicodeconverter.h"
#include "about.h"
#include <QApplication>
#include <map>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QInputDialog>
#include <QDebug>
#include <iostream>
#include <QMap>
#include <QComboBox>
#include <QIcon>
#include <QtPrintSupport>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QSettings settings("freaknz-qt");

    ui->fontSize->setValue(settings.value("settings/spinValue","14").toInt());
    ui->comboBox->setCurrentText(settings.value("settings/comboText","ML-TTKarthika").toString());


    ui->textEditUnicode->selectAll();
    ui->textEditUnicode->setFontPointSize(settings.value("settings/spinValue","14").toInt());
    ui->textEditAscii->selectAll();
    ui->textEditAscii->setFontPointSize(settings.value("settings/spinValue","14").toInt());


    ui->textEditAscii->setFont(settings.value("settings/comboText","ML-TTKarthika").toString());

    ui->textEditUnicode->setFont(QFont("Rachana"));


    ui->textEditUnicode->hide();
    ui->actionASCIIEditor2->hide();

    ui->cleanButton->hide();
    ui->actionNormal_Screen->setVisible(false);
//    ui->fontSize->setSuffix("pt");
    ui->fontSize->setRange(6,30);
    ui->fontSize->setSingleStep(2);


    ui->asciiError->setStyleSheet("QLabel { padding:5px; background-color : red; color : white; }");
    ui->labelAscii->setStyleSheet("QLabel {padding:5px; background-color:#34402a; color:white; font-weight:bold; border-radius:3px;}");
    ui->labelUnicode->setStyleSheet("QLabel {padding:5px; background-color:#795471; color:white; font-weight:bold;border-radius:3px;}");

    ui->asciiError->hide();
    ui->line->hide();
    ui->labelUnicode->hide();

    statusBar()->showMessage(wordCnt+"     "+characterCnt);

    ui->actionUnicodeEditor->setIcon(QIcon(path+"icons/48px-Unicode-Mode.svg.png"));
    ui->actionASCIIEditor->setIcon(QIcon(path+"icons/48px-ASCII-Mode.svg.png"));
    ui->actionOpen->setIcon(QIcon::fromTheme("document-open", QIcon(path+"icons/48px-Document-open.svg.png")));
    ui->actionSave->setIcon(QIcon::fromTheme("document-save", QIcon(path+"icons/48px-Document-save.svg.png")));
    ui->actionSave_As->setIcon(QIcon::fromTheme("document-save-as", QIcon(path+"icons/48px-Document-save-as.svg.png")));
    ui->actionUndo->setIcon(QIcon::fromTheme("edit-undo", QIcon(path+"icons/48px-Edit-undo.svg.png")));
    ui->actionRedo->setIcon(QIcon::fromTheme("edit-redo", QIcon(path+"icons/48px-Edit-redo.svg.png")));
    ui->actionPaste->setIcon(QIcon::fromTheme("edit-paste", QIcon(path+"icons/48px-Edit-paste.svg.png")));
    ui->actionCopy->setIcon(QIcon::fromTheme("edit-copy", QIcon(path+"icons/48px-Edit-copy.svg.png")));
    ui->actionClear->setIcon(QIcon::fromTheme("edit-clear", QIcon(path+"icons/48px-Edit-clear.svg.png")));
    ui->actionPrint->setIcon(QIcon::fromTheme("document-print", QIcon(path+"icons/48px-Document-print.svg.png")));
    ui->actionFull_Screen->setIcon(QIcon::fromTheme("view-fullscreen", QIcon(path+"icons/48px-View-fullscreen.svg.png")));
    ui->actionNormal_Screen->setIcon(QIcon::fromTheme("view-normal", QIcon(path+"icons/48px-View-normal.svg.png")));
    ui->actionQuit->setIcon(QIcon::fromTheme("application-exit", QIcon(path+"icons/48px-Process-stop.svg.png")));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(on_actionFontChange(int)));
    connect(ui->actionFull_Screen, SIGNAL(triggered()), this, SLOT(on_actionFull_Screen_triggered()));
    connect(ui->actionNormal_Screen, SIGNAL(triggered()), this, SLOT(on_actionNormal_Screen_triggered()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(on_actionExit_triggered()));
    connect(ui->cleanButton, SIGNAL(clicked()), this, SLOT(on_actionCleanText_triggered()));
    connect(ui->actionASCIIEditor2, SIGNAL(clicked()), this, SLOT(on_actionASCIIEditor_triggered()));
    connect(ui->actionUnicodeEditor2, SIGNAL(clicked()), this, SLOT(on_actionUnicodeEditor_triggered()));

    connect(ui->actionAsciiToUnicodeConvert, SIGNAL(clicked()), this, SLOT(on_actionAsciiToUnicodeConvert_triggered()));
    connect(ui->fontSize, SIGNAL(valueChanged(int)), this, SLOT(on_actionFontSizeChange(int)));
    connect(ui->textEditAscii,SIGNAL(cursorPositionChanged()),this,SLOT(on_actionCharChange()));

}


MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_actionAbout_triggered()
{

    About about;
    about.resize(320,240);
    about.setWindowTitle("About");
    about.setModal(true);

    about.exec();
}

void MainWindow::on_actionCharChange()
{
    QTextCursor cursor = ui->textEditAscii->textCursor();
    cursor.select(QTextCursor::WordUnderCursor);
    QString c = cursor.selectedText().right(1);
    if(c.contains(QRegularExpression(QStringLiteral("[^\\x{0000}-\\x{007F}]")))) {
        ui->asciiError->show();
        ui->line->show();

    } else {
        ui->asciiError->hide();
        ui->line->hide();
}

}

void MainWindow::on_actionClear_triggered()
{
    if (samW == "Unicode") {
        ui->textEditUnicode->clear();
    } else {
        ui->textEditAscii->clear();
    }
}

void MainWindow::on_actionFontSizeChange(int index)
{
        ui->textEditUnicode->selectAll();
        ui->textEditUnicode->setFontPointSize(index);
        ui->textEditAscii->selectAll();
        ui->textEditAscii->setFontPointSize(index);

}

void MainWindow::on_actionUnicodeEditor_triggered()
{
    ui->actionUnicodeEditor2->hide();
    ui->actionASCIIEditor2->show();
    samW = "Unicode";
    ui->textEditUnicode->show();
    ui->textEditAscii->hide();
    ui->comboBox->hide();
    ui->cleanButton->show();
    ui->actionAsciiToUnicodeConvert->hide();
    ui->asciiError->hide();
    ui->line->hide();
    ui->labelAscii->hide();
    ui->labelUnicode->show();

}

void MainWindow::on_actionASCIIEditor_triggered()
{
    ui->actionASCIIEditor2->hide();
    ui->actionUnicodeEditor2->show();
    ui->comboBox->show();
    ui->cleanButton->hide();
    ui->actionAsciiToUnicodeConvert->show();
    ui->textEditUnicode->hide();
    ui->textEditAscii->show();
    samW = "";
    ui->labelUnicode->hide();
    ui->labelAscii->show();


}

void MainWindow::on_actionFull_Screen_triggered()
{
    showFullScreen();
    ui->actionNormal_Screen->setVisible(true);
    ui->actionFull_Screen->setVisible(false);

}

void MainWindow::on_actionNormal_Screen_triggered()
{
    showNormal();
    ui->actionNormal_Screen->setVisible(false);
    ui->actionFull_Screen->setVisible(true);
}

void MainWindow::on_actionFontChange(int)
{
    QFont changeFont(ui->comboBox->currentText());
    ui->textEditAscii->setFont(changeFont);
}

void MainWindow::on_actionCleanText_triggered()
{
    QString asciiText = ui->textEditUnicode->toPlainText();
    ui->textEditUnicode->clear();
    asciiText = asciiText.remove("￼");

    asciiText = asciiText.replace("\n","ก");
    asciiText = asciiText.simplified();
    asciiText = asciiText.replace("ก","\n");


            QString symbols(".,;:!?");
            QString cleanText;

        for (QString::size_type i = 0; i < asciiText.length(); i++){
      QString a = asciiText.mid(i,1);
      QString b = asciiText.mid(i+1,1);

      if (a=="\n" && b=="\n"){

      }
      else if(symbols.contains(a) && b != " ") {

          cleanText.append(a+" ");
      }      else if (a == " " && symbols.contains(b)) {
          cleanText.append(b);
          i=i+1;
      }
      else if(a=="‌"){
      }

      else {
          cleanText.append(a);
      }

        }
//        cleanText.replace("\n","\n\n");
        cleanText.replace("ോ","ോ");
        cleanText.replace("ൊ","ൊ");


        QFile zwnjReplace(path+"map/zwnj.map");
        if (!zwnjReplace.open(QIODevice::ReadOnly | QIODevice::Text))
            return;


        while (!zwnjReplace.atEnd()) {
            QString line = zwnjReplace.readLine();
            line.unicode();
            auto parts = line.split("=");
            QString lhs = line.split("=")[0];
            QString rhs = line.split("=")[1];
            lhs = lhs.simplified();
            rhs = rhs.simplified();
            cleanText.replace(lhs,rhs);
        }


        ui->textEditUnicode->setText(cleanText);

}

void MainWindow::on_actionUndo_triggered()
{
    if (samW == "Unicode") {
        ui->textEditUnicode->undo();
    } else {
        ui->textEditAscii->undo();
    }
}

void MainWindow::on_actionRedo_triggered()
{
    if (samW == "Unicode") {
        ui->textEditUnicode->redo();
    } else {
        ui->textEditAscii->redo();
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Open a file");
    QFile file (file_name);
    QString file_path=file_name;

    if(!file.open(QFile::ReadOnly | QFile::Text)){
        QMessageBox::critical(this,"Error Opening File","File Cannot be Opened!");
        return;
    }
    else{
        //Reading the file
        QTextStream inputData(&file);
        QString fileText = inputData.readAll();
        if (samW == "Unicode") {
            ui->textEditUnicode->setHtml(fileText);
        } else {
            ui->textEditAscii->setHtml(fileText);
        }
        file.close();
    }
}

void MainWindow::on_actionPaste_triggered()
{
    if (samW == "Unicode") {
        ui->textEditUnicode->paste();
    } else {
        QClipboard *clipboard = QGuiApplication::clipboard();
        QString originalText = clipboard->text();
        ui->textEditAscii->insertHtml(originalText);
    }
}

void MainWindow::on_actionCopy_triggered()
{
    QClipboard *p_Clipboard = QApplication::clipboard();
    if (samW == "Unicode") {
        p_Clipboard->setText(ui->textEditUnicode->toPlainText());
        } else {
        p_Clipboard->setText(ui->textEditAscii->toPlainText());
        }
    }

void MainWindow::on_actionSave_As_triggered()
{

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save File"), "",
                                                    tr("Text Files (*.txt);;All Files (*)"));

    if (fileName.section('.',-1)!="txt"){
    fileName.append(".txt");
}
    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        } else {

            QTextStream out(&file);
            if (samW == "Unicode") {
                out << ui->textEditUnicode->toPlainText();
            } else {
                out << ui->textEditAscii->toPlainText();
            }
            }
    }


}

void MainWindow::on_actionSave_triggered()
{
    if (samW == "Unicode") {

    if (fileNameUnicode.isEmpty()) {

        fileNameUnicode.append(QFileDialog::getSaveFileName(this,
                                                     tr("Save File"), "",
                                                     tr("Text Files (*.txt);;All Files (*)")));

        if (fileNameUnicode.section('.',-1)!="txt"){
        fileNameUnicode.append(".txt");
    }    }

    QFile file(fileNameUnicode);
    if (fileNameUnicode.isEmpty())
        return;
    else {

        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        } else {

            QTextStream out(&file);
            out << ui->textEditUnicode->toPlainText();

        }
    }
    } else {

        if (fileNameAscii.isEmpty()) {

            fileNameAscii.append(QFileDialog::getSaveFileName(this,
                                                         tr("Save File"), "",
                                                         tr("Text Files (*.txt);;All Files (*)")));
            if (fileNameAscii.section('.',-1)!="txt"){
            fileNameAscii.append(".txt");
        }         }

        QFile file(fileNameAscii);
        if (fileNameAscii.isEmpty())
            return;
        else {

            if (!file.open(QIODevice::WriteOnly)) {
                QMessageBox::information(this, tr("Unable to open file"),
                                         file.errorString());
                return;
            } else {

                QTextStream out(&file);
                out << ui->textEditAscii->toPlainText();
            }
        }
    }
}

void MainWindow::on_actionPrint_triggered()
{
    QPrinter printer;
    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("Print Document"));

    if (dialog->exec() != QDialog::Accepted){
        return;
    }
    else {
        if (samW == "Unicode") {
            ui->textEditUnicode->print(&printer);
        } else {
            ui->textEditAscii->print(&printer);
        }
        dialog->close();
        return;
    }
}

void MainWindow::on_actionExit_triggered()
{
    QSettings settings("freaknz-qt");
    settings.setValue("settings/spinValue",ui->fontSize->value());
    settings.setValue("settings/comboText",ui->comboBox->currentText());
    QApplication::quit();

}
void MainWindow::on_actionAsciiToUnicodeConvert_triggered()
{

    asciiToUnicodeConverter asciiConvereter;
    QString asciiText;
    QString fontName;
    QString unicodeOut;


    QTextBlock currentBlock = ui->textEditAscii->document()->firstBlock();

    while (currentBlock.isValid()) {

        QTextBlockFormat blockFormat = currentBlock.blockFormat();
        QTextCharFormat charFormat = currentBlock.charFormat();
        QFont font = charFormat.font();

        QTextBlock::iterator it;
        for (it = currentBlock.begin(); !(it.atEnd()); ++it) {

            QTextFragment currentFragment = it.fragment();
            if (currentFragment.isValid()) {
                // a text fragment also has a char format with font:
                QTextCharFormat fragmentCharFormat = currentFragment.charFormat();
                QFont fragmentFont = fragmentCharFormat.font();
                // etc...

                 QString availableFonts = "Haritha,ML-TTNandini,Manorama,ML-TTAmbili,ML-TTRevathi,Matweb,ML-TTKarthika,MLB-TTIndulekha,TM-TTValluvar";
                 QStringList availableFontList = availableFonts.split(',', QString::SkipEmptyParts);
                 QString asciiText = currentFragment.text();



                 if (fragmentFont.family().split(",")[0].contains("ML")) {
                     hasML = "yes";

                 if (availableFontList.contains(fragmentFont.family().split(",")[0])){

                     QString fontName = fragmentFont.family().split(",")[0];

                     unicodeOut.append(asciiConvereter.asciiToUnicodeConverterFunction(asciiText, fontName));



                 } else {


                     fontName = ui->comboBox->currentText();
                     unicodeOut.append(asciiConvereter.asciiToUnicodeConverterFunction(asciiText, fontName));
}
            }
                  else {
                     unicodeOut = unicodeOut.append(asciiText);
}
             }


        }

       unicodeOut = unicodeOut.append("<br/>");
       currentBlock = currentBlock.next();
     }

//    qDebug()<<hasML;

    if (hasML.isNull()) {


        hasML = "";
        unicodeOut.clear();


        asciiText = ui->textEditAscii->toPlainText();
        fontName = "ML-TTKarthika";
        unicodeOut = asciiConvereter.asciiToUnicodeConverterFunction(asciiText, fontName);
    }

    ui->textEditUnicode->show();
    ui->textEditAscii->hide();
    ui->textEditUnicode->setHtml(unicodeOut);
    ui->actionUnicodeEditor2->hide();
    ui->actionASCIIEditor2->show();
    ui->comboBox->hide();
    ui->cleanButton->show();
    ui->actionAsciiToUnicodeConvert->hide();
    ui->asciiError->hide();
    ui->line->hide();
    ui->labelAscii->hide();
    ui->labelUnicode->show();
    samW = "Unicode";
    //to show word count

    wordCnt = "Word Count: "+QString::number(unicodeOut.count(" ")+unicodeOut.count("\n")+1);
    characterCnt = "Letter Count: "+QString::number(unicodeOut.length());
    statusBar()->showMessage(wordCnt+"     "+characterCnt);

}
